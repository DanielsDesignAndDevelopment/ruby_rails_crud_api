# Marko's Ultimate README
```bash
OH YEAHHHH!
```
## Ruby on Rails

Create Rails App:
```bash
rails new my_app
```
With API tools:
```bash
rails new myApp --api
```
Installing Gems:
```bash
gem install myGem  //Then add to Gemfile
```
Installing Gems as development dependecies:
```ruby
# Gemfile
group :development do
  gem "my_gem"
end
```
```bash
bundle install
```
Run Rails Server:
```bash
rails server  //Runs on 3000 by default
```
Generate Model:
```bash
rails generate model MyModel my_title:string my_body:text
```
Run migration:
```bash
rails db:migrate
```

Seeding a database with fake data:
```ruby
# Faker gem installed as a dev dependency
# https://github.com/faker-ruby/faker

# db/seeds.rb
5.times do
  Article.create({
    title: Faker::Book.title,
    body: Faker::Lorem.sentence
  })
end
```
```bash
rails db:seed
```
Viewing a sqlite3 database table
```bash
sqlite3
```
```bash
.open db/development.sqlite3
```
```bash
.tables //Lists tables
```
```bash
.schema table_name //Shows table schema
```
```bash
SELECT * FROM table_name; //Prints entire table
```
```bash
.help //lists commands
```
View routes
```bash
rails routes
```